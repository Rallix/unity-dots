using Unity.Entities;
using DOTS.Components.Interaction;
using DOTS.Components.Physics;

namespace DOTS.Systems
{
    public class DamageSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            float deltaTime = Time.DeltaTime;

            Entities.ForEach((DynamicBuffer<CollisionBuffer> collisions, ref Health health) =>
            {
                for (int i = 0; i < collisions.Length; i++)
                {
                    if (health.invincibilityTimer <= 0 && HasComponent<Damage>(collisions[i].entity))
                    {
                        health.value -= GetComponent<Damage>(collisions[i].entity).value;
                        health.invincibilityTimer = health.invincibility;
                    }
                }
            }).Schedule();

            Entities.WithNone<Kill>()
                    .ForEach((Entity entity, ref Health health) =>
                     {
                         health.invincibilityTimer -= deltaTime;
                         if (health.value <= 0)
                         {
                             EntityManager.AddComponentData(entity, new Kill {timer = health.killTimer});
                         }
                     }).WithStructuralChanges().Run(); // add or remove entity

            var ecbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            EntityCommandBuffer.ParallelWriter ecb = ecbSystem.CreateCommandBuffer().AsParallelWriter();
            Entities.ForEach((Entity entity, int entityInQueryIndex, ref Kill kill) =>
            {
                kill.timer -= deltaTime;
                if (kill.timer <= 0)
                {
                    ecb.DestroyEntity(entityInQueryIndex, entity);
                }
            }).Schedule();
            ecbSystem.AddJobHandleForProducer(this.Dependency);
        }
    }
}
