using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;

using DOTS.Components.Physics;

namespace DOTS.Systems
{
    public class CollisionSystem : SystemBase
    {
        struct CollisionSystemJob : ICollisionEventsJob
        {
            public BufferFromEntity<CollisionBuffer> collisions;

            public void Execute(CollisionEvent collisionEvent)
            {
                if (collisions.HasComponent(collisionEvent.EntityA))
                {
                    collisions[collisionEvent.EntityA].Add(new CollisionBuffer { entity = collisionEvent.EntityB });
                }

                if (collisions.HasComponent(collisionEvent.EntityB))
                {
                    collisions[collisionEvent.EntityB].Add(new CollisionBuffer { entity = collisionEvent.EntityA });
                }
            }
        }

        struct TriggerSystemJob : ITriggerEventsJob
        {
            public BufferFromEntity<TriggerBuffer> triggers;

            public void Execute(TriggerEvent triggerEvent)
            {
                if (triggers.HasComponent(triggerEvent.EntityA))
                {
                    triggers[triggerEvent.EntityA].Add(new TriggerBuffer { entity = triggerEvent.EntityB });
                }
                
                if (triggers.HasComponent(triggerEvent.EntityB))
                {
                    triggers[triggerEvent.EntityB].Add(new TriggerBuffer { entity = triggerEvent.EntityA });
                }
            }
        }

        protected override void OnUpdate()
        {
            PhysicsWorld physics = World.GetOrCreateSystem<BuildPhysicsWorld>().PhysicsWorld;
            ISimulation simulation = World.GetOrCreateSystem<StepPhysicsWorld>().Simulation;

            //! Collisions
            Entities.ForEach((DynamicBuffer<CollisionBuffer> collisions) => { collisions.Clear(); }).Run();
            JobHandle collisionJob = new CollisionSystemJob
            {
                    collisions = GetBufferFromEntity<CollisionBuffer>()
            }.Schedule(simulation, ref physics, this.Dependency);
            collisionJob.Complete();

            //! Triggers
            Entities.ForEach((DynamicBuffer<TriggerBuffer> triggers) => { triggers.Clear(); }).Run();
            JobHandle triggerJob = new TriggerSystemJob
            {
                    triggers = GetBufferFromEntity<TriggerBuffer>()
            }.Schedule(simulation, ref physics, this.Dependency);
            triggerJob.Complete();
        }
    }
}
 