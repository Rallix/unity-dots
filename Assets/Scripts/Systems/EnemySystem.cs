using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;

using DOTS.Components;

namespace DOTS.Systems
{
    public class EnemySystem : SystemBase
    {
        public const float CellSize = 1f;
        const float Epsilon = 0.000_001f;
        static readonly float DistanceLimit = CellSize - Epsilon;

        Random random = new Random(1234);

        protected override void OnUpdate()
        {
            // Cache for scheduling
            Random rand = random;
            random.NextInt();

            var raycaster = new MovementRaycast
            {
                    physics = World.GetOrCreateSystem<BuildPhysicsWorld>().PhysicsWorld,
            };

            Entities.ForEach((ref Movable movable, ref Enemy enemy, ref Translation translation) =>
            {
                float distance = math.distance(translation.Value, enemy.previousCell);
                if (distance >= DistanceLimit)
                {
                    enemy.previousCell = math.round(translation.Value);
                    var directions = new NativeList<float3>(Allocator.Temp)
                    {
                            new float3(0, 0, -1),
                            new float3(0, 0, 1),
                            new float3(1, 0, 0),
                            new float3(-1, 0, 0)
                    };
                    for (int i = directions.Length - 1; i >= 0; i--) // reverse (deleting elements)
                    {
                        float3 direction = directions[i];
                        if (direction.Equals(-movable.direction)
                         || raycaster.CheckCollision(translation.Value, direction, movable.direction))
                        {
                            directions.RemoveAt(i); // opposite direction OR wall
                        }
                    }
                    movable.direction = directions[rand.NextInt(directions.Length)];
                    directions.Dispose();
                }
            }).Schedule(); //.WithoutBurst().Run(); -- if a function is needed
        }

        struct MovementRaycast
        {
            [ReadOnly] public PhysicsWorld physics;

            public bool CheckCollision(float3 rayPosition, float3 rayDirection, float3 currentDirection)
            {
                var ray = new RaycastInput
                {
                        Start = rayPosition,
                        End = rayPosition + (rayDirection * (DistanceLimit)),
                        Filter = new CollisionFilter
                        {
                                GroupIndex = 0,
                                BelongsTo = 1u << 1,   // Enemy
                                CollidesWith = 1u << 2 // Player 
                        }
                };
                return physics.CastRay(ray);
            }
        }
    }
}
