using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

using DOTS.Components;

namespace DOTS.Systems
{
    public class PlayerSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            Entities
                   .WithAll<Player>()
                   .ForEach((ref Movable movable) => { movable.direction = new float3(input.x, 0, input.y); }).Schedule();
        }
    }
}
