using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;

using DOTS.Components;

namespace DOTS.Systems
{
    public class MovableSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref PhysicsVelocity velocity, in Movable movable) =>
            {
                float3 step = movable.direction * movable.speed;
                velocity.Linear = step;
            }).Schedule();
        }
    }
}
