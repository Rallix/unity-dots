using Unity.Entities;
using Unity.Mathematics;

namespace DOTS.Components
{
    [GenerateAuthoringComponent]
    public struct Movable : IComponentData
    {
        public float speed;
        public float3 direction;
    }
}
