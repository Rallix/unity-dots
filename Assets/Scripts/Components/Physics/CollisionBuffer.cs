﻿using Unity.Entities;

namespace DOTS.Components.Physics
{
    public struct CollisionBuffer : IBufferElementData
    {
        public Entity entity;
    }
}
