﻿using Unity.Entities;

namespace DOTS.Components.Physics
{
    public struct TriggerBuffer : IBufferElementData
    {
        public Entity entity;
    }
}
