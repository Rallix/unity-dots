using Unity.Entities;

namespace DOTS.Components.Interaction
{
    [GenerateAuthoringComponent]
    public struct Damage : IComponentData
    {
        public float value;
    }
}
