using Unity.Entities;

namespace DOTS.Components.Interaction
{
    [GenerateAuthoringComponent]
    public struct Kill : IComponentData
    {
        public float timer;
    }
}
