using Unity.Entities;
using UnityEngine;

namespace DOTS.Components.Interaction
{
    [GenerateAuthoringComponent]
    public struct Health : IComponentData
    {
        public float value;
        public float invincibility;
        public float invincibilityTimer; // [HideInInspector] 
        public float killTimer;
    }
}
