using Unity.Entities;
using Unity.Mathematics;

namespace DOTS.Components
{
    [GenerateAuthoringComponent]
    public struct Enemy : IComponentData
    {
        public float3 previousCell;
    }
}
