using UnityEngine;
using Unity.Entities;

namespace DOTS.Components.Physics
{
    [DisallowMultipleComponent]
    [RequiresEntityConversion]
    public class PhysicsEventsAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddBuffer<CollisionBuffer>(entity);
            dstManager.AddBuffer<TriggerBuffer>(entity);
        }
    }
}
